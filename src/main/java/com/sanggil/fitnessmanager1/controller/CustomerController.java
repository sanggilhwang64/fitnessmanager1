package com.sanggil.fitnessmanager1.controller;

import com.sanggil.fitnessmanager1.model.CustomerInfoUpdateRequest;
import com.sanggil.fitnessmanager1.model.CustomerItem;
import com.sanggil.fitnessmanager1.model.CustomerRequest;
import com.sanggil.fitnessmanager1.model.CustomerWeightUpdateRequest;
import com.sanggil.fitnessmanager1.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }

    @PutMapping("/info/id/{id}")
    public String putCustomerInfo(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdateRequest request) {
        customerService.putCustomerInfo(id, request);

        return "OK";
    }

    @PutMapping("/weight/id/{id}")
    public String putCustomerWeight(@PathVariable long id, @RequestBody @Valid CustomerWeightUpdateRequest request) {
        customerService.putCustomerWeight(id, request);

        return "OK";
    }

    @PutMapping("/visit/id/{id}")
    public String putCustomerVisit(@PathVariable long id) {
        customerService.putCustomerVisit(id);

        return "OK";
    }
}
