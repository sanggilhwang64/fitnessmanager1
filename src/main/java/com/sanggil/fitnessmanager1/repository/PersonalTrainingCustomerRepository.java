package com.sanggil.fitnessmanager1.repository;

import com.sanggil.fitnessmanager1.entity.PersonalTrainingCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalTrainingCustomerRepository extends JpaRepository<PersonalTrainingCustomer, Long> {
}
